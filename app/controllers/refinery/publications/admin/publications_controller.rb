module Refinery
  module Publications
    module Admin
      class PublicationsController < ::Refinery::AdminController

        crudify :'refinery/publications/publication',
                :title_attribute => 'publication'

        private

        # Only allow a trusted parameter "white list" through.
        def publication_params
          params.require(:publication).permit(:publication, :article_title, :date, :url, :photo_id, :description)
        end
      end
    end
  end
end
