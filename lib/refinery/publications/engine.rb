module Refinery
  module Publications
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Publications

      engine_name :refinery_publications

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "publications"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.publications_admin_publications_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Publications)
      end
    end
  end
end
