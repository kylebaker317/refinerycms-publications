# Publications extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/publications
    gem build refinerycms-publications.gemspec
    gem install refinerycms-publications.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-publications.gem
