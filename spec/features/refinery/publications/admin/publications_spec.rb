# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "Publications" do
    describe "Admin" do
      describe "publications", type: :feature do
        refinery_login

        describe "publications list" do
          before do
            FactoryGirl.create(:publication, :publication => "UniqueTitleOne")
            FactoryGirl.create(:publication, :publication => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.publications_admin_publications_path
            expect(page).to have_content("UniqueTitleOne")
            expect(page).to have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.publications_admin_publications_path

            click_link "Add New Publication"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Publication", :with => "This is a test of the first string field"
              expect { click_button "Save" }.to change(Refinery::Publications::Publication, :count).from(0).to(1)

              expect(page).to have_content("'This is a test of the first string field' was successfully added.")
            end
          end

          context "invalid data" do
            it "should fail" do
              expect { click_button "Save" }.not_to change(Refinery::Publications::Publication, :count)

              expect(page).to have_content("Publication can't be blank")
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:publication, :publication => "UniqueTitle") }

            it "should fail" do
              visit refinery.publications_admin_publications_path

              click_link "Add New Publication"

              fill_in "Publication", :with => "UniqueTitle"
              expect { click_button "Save" }.not_to change(Refinery::Publications::Publication, :count)

              expect(page).to have_content("There were problems")
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:publication, :publication => "A publication") }

          it "should succeed" do
            visit refinery.publications_admin_publications_path

            within ".actions" do
              click_link "Edit this publication"
            end

            fill_in "Publication", :with => "A different publication"
            click_button "Save"

            expect(page).to have_content("'A different publication' was successfully updated.")
            expect(page).not_to have_content("A publication")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:publication, :publication => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.publications_admin_publications_path

            click_link "Remove this publication forever"

            expect(page).to have_content("'UniqueTitleOne' was successfully removed.")
            expect(Refinery::Publications::Publication.count).to eq(0)
          end
        end

      end
    end
  end
end
