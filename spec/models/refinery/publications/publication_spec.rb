require 'spec_helper'

module Refinery
  module Publications
    describe Publication do
      describe "validations", type: :model do
        subject do
          FactoryGirl.create(:publication,
          :publication => "Refinery CMS")
        end

        it { should be_valid }
        its(:errors) { should be_empty }
        its(:publication) { should == "Refinery CMS" }
      end
    end
  end
end
