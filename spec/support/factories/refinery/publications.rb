
FactoryGirl.define do
  factory :publication, :class => Refinery::Publications::Publication do
    sequence(:publication) { |n| "refinery#{n}" }
  end
end

