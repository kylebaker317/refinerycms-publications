Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  publication_body = File.open('vendor/extensions/publications/db/old_files/sophisticated_living_excerpt.html', 'rb').read
  Refinery::Publications::Publication.create publication: 'Sophisticated Living',
                                             article_title: 'Holiday at Home',
                                             url: 'http://digital.slmag.net/i/71630-jul-aug-2012',
                                             date: '2012-07-25',
                                             description: publication_body

  def fake_publications(publications)
    create_pub = publications - Refinery::Publications::Publication.all.count
    create_pub.times do
      Refinery::Publications::Publication.create publication: Faker::Book.publisher,
                                                 article_title: Faker::Book.title,
                                                 url: Faker::Internet.url,
                                                 date: Faker::Date.between(17.years.ago, 1.month.ago),
                                                 description: Faker::Lorem.paragraph
    end
    pub_count = Refinery::Publications::Publication.all.count
    if create_pub < 0
      puts "0 publications added."
    else
      puts "#{create_pub} publications added."
    end
    puts "Currently there is/are #{pub_count} publication(s)"

  end

  fake_publications(10)
  puts 'Refinery::Publications dummy_data.rb ran successfully'
end